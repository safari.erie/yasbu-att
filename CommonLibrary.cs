﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Net;
using System.Text;

namespace AttLogs
{
    class CommonLibrary
    {
        public string server = AttLogs.Properties.Settings.Default.Server;
        public string email = AttLogs.Properties.Settings.Default.Email;
        public string phone = AttLogs.Properties.Settings.Default.Phone;
        public string companyName = AttLogs.Properties.Settings.Default.CompanyName;

        public string controller = "AttendanceBatch";
        public string companyId;
        private static CommonLibrary instance = null;

        public CommonLibrary() {
            companyId = getCompanyId();
        }

        #region 
        public static CommonLibrary Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new CommonLibrary();
                }
                return instance;
            }
        }
        #endregion

        #region get Ip Machine
        public List<ComboboxItem> getIpMachine()
        {
            string param = "?companyid=" + companyId;

            List<ComboboxItem> ip = new List<ComboboxItem>();
            ComboboxItem ci = new ComboboxItem();
            var result = this.GET("getIpMachine", param);

            string[] data = result.Split(';');
            if (data.Length > 0) {
                foreach (string row in data)
                {
                    string[] idx = row.Split('~');
                    ci.Value = row;
                    ci.Text = idx[0];
                    ip.Add(ci);
                }
            }
            return ip;
        }
        #endregion

        #region get Withdrawal Time
        public List<string> getWithdrawalTime()
        {
            string param = "?companyid=" + companyId;

            List<string> listTime = new List<string>();
            var result = this.GET("getWithdrawalTime", param);

            string[] data = result.Split(';');
            if (data.Length > 0)
            {
                foreach (string row in data)
                {
                    listTime.Add(row);
                }
            }
            return listTime;
        }
        #endregion

        #region get Company Id
        public string getCompanyId()
        {
            string param = "?email=" + email + "&phone=" + phone + "&companyname=" + companyName;
            var result = this.GET("getCompanyId", param);
            return result.ToString();
        }
        #endregion

        #region Create Log File
        public void createLogFile(string name, string ip)
        {
            try
            {
                if (!File.Exists(name))
                {
                    File.Create(name).Dispose();

                    using (StreamWriter sr = new StreamWriter(name, false))
                    {
                        //sr.WriteLine("Pull from " + ip);
                        sr.WriteLine("enroll_no~attendance_dt~inout_status~ip_machine");
                    }
                }
                else
                {
                    File.Create(name);
                    TextWriter tw = new StreamWriter(name);
                    //tw.WriteLine("Pull from " + ip);
                    tw.WriteLine("enroll_no~attendance_dt~inout_status~ip_machine");
                    tw.Close();
                }
            }
            catch (Exception e)
            {
                
            }

        }
        #endregion

        public void writeLog(string path, string msg)
        {
            using (var tw = new StreamWriter(path, true))
            {
                tw.WriteLine(msg);
                tw.Close();
            }
        }


        #region Write Log Application
        public void writeLogApp(string messages)
        {
            string apppath = System.Windows.Forms.Application.StartupPath.ToString();
            string name = apppath + @"\log\Log_Debug_" + DateTime.Now.ToString("yyyyMMdd") + ".txt";

            try
            {
                if (!File.Exists(name))
                {
                    File.Create(name).Dispose();
                    using (StreamWriter sr = new StreamWriter(name, false))
                    {
                        sr.WriteLine(messages);
                    }
                }
                else
                {
                    //File.Create(name);
                    TextWriter tw = new StreamWriter(name, true);
                    tw.WriteLine(messages);
                    tw.Close();
                }
            }
            catch (Exception e)
            {

            }

        }
        #endregion

        #region Get from server
        private string GET(string m, string args)
        {
            string url = server.ToString() + controller + "/" + m + args;
            
            //added 20180711
            // Ganti framewerk .net dari versi 2.0 ke 4.5
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            //added 20180711

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            try
            {
                WebResponse response = request.GetResponse();
                using (Stream responseStream = response.GetResponseStream())
                {
                    StreamReader reader = new StreamReader(responseStream, Encoding.UTF8);
                    return reader.ReadToEnd();
                }
            }
            catch (WebException ex)
            {
                string messages = "[" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "] - [ERR] : " + ex.Message.ToString();
                writeLogApp(messages);

                WebResponse errorResponse = ex.Response;
                using (Stream responseStream = errorResponse.GetResponseStream())
                {
                    StreamReader reader = new StreamReader(responseStream, Encoding.GetEncoding("utf-8"));
                    String errorText = reader.ReadToEnd();
                    // log errorText
                }
                //throw;
                return "0";
            }
        }
        #endregion

        #region Push to server
        public bool pushData(string m, string data)
        {
            bool result = false;
            string url = server.ToString() + controller + "/" + m;

            using (WebClient client = new WebClient())
            {
                try
                {
                    NameValueCollection postData = new NameValueCollection(){
                          { "data", data },
                          { "companyid", companyId }
                    };
                    string res = Encoding.UTF8.GetString(client.UploadValues(url, postData));
                    result = (res == "0") ? true : false ;
                }
                catch (Exception e) {
                    string messages = "[" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "] - [ERR] : " + e.Message.ToString();
                    writeLogApp(messages);
                    result = false;
                }
            }
            return result;
        }
        #endregion

        #region sync Transaction Attendance
        public bool processTransAttendance(string m, string dateFromTo)
        {
            bool result = false;
            string url = server.ToString() + controller + "/" + m;

            using (WebClient client = new WebClient())
            {
                try
                {
                    NameValueCollection postData = new NameValueCollection(){
                          { "data", dateFromTo },
                        //{ "companyid", "1" }
                        { "companyid",companyId }
                    };
                    string res = Encoding.UTF8.GetString(client.UploadValues(url, postData));
                    result = (res == "1") ? true : false;
                }
                catch (Exception e)
                {
                    string messages = "[" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "] - [ERR] : " + e.Message.ToString();
                    writeLogApp(messages);
                    result = false;
                }
            }
            return result;
        }
        #endregion

    }
}
