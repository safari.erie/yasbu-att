﻿using AttLogs.Properties;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace AttLogs
{
    class ContextMenus
    {
        bool isAboutLoaded = false;
        bool isAttLog = false;

        #region Create Menu
        public ContextMenuStrip Create()
        {
            // Add the default menu options.
            ContextMenuStrip menu = new ContextMenuStrip();
            ToolStripMenuItem item;
            ToolStripSeparator sep;

            // Manual Pull & Push
            item = new ToolStripMenuItem();
            item.Text = "Manual";
            item.Click += new EventHandler(Manual_Click);
            item.Image = Resources.Download2;
            menu.Items.Add(item);

            // Pull & Push Attendance
            item = new ToolStripMenuItem();
            item.Text = "Attendance";
            item.Click += new EventHandler(AttendanceLog_Click);
            item.Image = Resources.Download;
            menu.Items.Add(item);

            // Push Attendance Realtime 
            item = new ToolStripMenuItem();
            item.Text = "Attendance Realtime";
            item.Click += new EventHandler(AttendanceRealtime_Click);
            item.Image = Resources.Download2;
            menu.Items.Add(item);

            // Config.
            //item = new ToolStripMenuItem();
            //item.Text = "Config";
            //item.Click += new EventHandler(Config_Click);
            //item.Image = Resources.Explorer;
            //menu.Items.Add(item);

            // Separator.
            sep = new ToolStripSeparator();
            menu.Items.Add(sep);

            // Exit.
            item = new ToolStripMenuItem();
            item.Text = "Exit";
            item.Click += new System.EventHandler(Exit_Click);
            item.Image = Resources.Close;
            menu.Items.Add(item);

            return menu;
        }
        #endregion

        #region event
        void Manual_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Starting pull attendance!");
            Attendance a = new Attendance();

            new Thread(() =>
            {
                Thread.CurrentThread.IsBackground = true;
                /* run your code here */
                a.pullAttendance();

            }).Start();            
            //MessageBox.Show("Finish pull attendance!");
        }
        void AttendanceLog_Click(object sender, EventArgs e)
        {
            if (!isAttLog)
            {
                isAttLog = true;
                //new Login().ShowDialog();
                AttLogsMain x = new AttLogsMain();
                x.Show();
                //this.Hide();
                isAttLog = false;
            }
        }

        void AttendanceRealtime_Click(object sender,EventArgs e)
        {
            isAttLog = true;
            AttendanceRealtime atr = new AttendanceRealtime();
            atr.Show();
            isAttLog = false;

        }
        //void Config_Click(object sender, EventArgs e)
        //{
        //    if (!isAboutLoaded)
        //    {
        //        isAboutLoaded = true;
        //        new frmConfig().ShowDialog();
        //        isAboutLoaded = false;
        //    }
        //}

        void Exit_Click(object sender, EventArgs e)
        {
            // Quit without further ado.
            Application.Exit();
        }
        #endregion
    }
}
