﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
//using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace AttRealtime
{
    public class Program
    {

        string _httpString = "";
        
        //static Logger Log;
        public static bool threadOnProgress = false;
        public static string server = AttRealtime.Properties.Settings.Default.Server;
        public static string IPAddress = AttRealtime.Properties.Settings.Default.IPAddress;
        public static int Password = AttRealtime.Properties.Settings.Default.Password;
        public static int Port = AttRealtime.Properties.Settings.Default.Port;
        static IntPtr solutions_handle;
        static bool ConnectionStatus = false;
        static string LogFileName = "Log_Attendence";
        static List<AttendenceModel> attList = new List<AttendenceModel>();
        static AttendenceModel attRow;
        static string DeviceId;
        public zkemkeeper.CZKEMClass axCZKEM1 = new zkemkeeper.CZKEMClass();


        static void Main(string[] args)
        {
            
            //Thread thread = new Thread(sync);
           // Log = new Logger(LogFileName, "Attendence Personalia Log");
            //if(Connection() == true){
            //   // thread.Start();
            //    sync();
            //}

            
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new RTEventsMain());
            //Console.ReadKey();
        }

        static void sync()
        {
            zkemkeeper.CZKEMClass axCZKEM1 = new zkemkeeper.CZKEMClass();
            int iMachineNumber = 1;
             string sEnrollNumber = "";
            int iIsInValid = 0;
            int iAttState = 0;
            int iVerifyMethod = 0;
            int iYear = 0;
            int iMonth = 0;
            int iDay = 0;
            int iHour = 0;
            int iMinute = 0;
            int iSecond;
            int iWorkCode;
            Console.WriteLine("Connection...!!");
            if (axCZKEM1.RegEvent(iMachineNumber, 65535))
            {
              //  axCZKEM1.OnAttTransactionEx += new zkemkeeper._IZKEMEvents_OnAttTransactionExEventHandler(axCZKEM1_OnAttTransactionEx); 
            }
        }

        #region RealTime Events
        private void axCZKEM1_OnAttTransactionEx(string sEnrollNumber, int iIsInValid, int iAttState, int iVerifyMethod, int iYear, int iMonth, int iDay, int iHour, int iMinute, int iSecond, int iWorkCode)
        {
            string attendanceDate = "";


            int enrollNo = 0;
            int status = 0;

            enrollNo = Convert.ToInt32(sEnrollNumber);
            attendanceDate = string.Format("{0:yyyy-MM-dd}", DateTime.Parse(iYear.ToString() + "-" + iMonth.ToString() + "-" + iDay.ToString()));
            status = iWorkCode;
            _httpString = string.Format("{0}~{1}~{2}~{3}", enrollNo, attendanceDate, status, IPAddress);
        }
        #endregion

        static void push()
        {
            
        }

        static bool Connection()
        {
            bool con = false;
            if (IPAddress.ToString() != "" && Port.ToString() != "")
            {
               // Log.WriteLog(string.Format("Connecting to Machine [ Ip Addr : {0} Port : {1} ]", IPAddress, Port));
                 zkemkeeper.CZKEMClass axCZKEM1 = new zkemkeeper.CZKEMClass();
                bool bIsConnected = false;//the boolean value identifies whether the device is connected
                int Password = AttRealtime.Properties.Settings.Default.Password;
                axCZKEM1.SetCommPassword(Password);
                bIsConnected = axCZKEM1.Connect_Net(IPAddress.ToString(), Port);
                con = bIsConnected;
            }
            return con;
        }
    }
}
