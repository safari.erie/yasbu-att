﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
//using System.Diagnostics;
using System.Windows.Forms;

namespace AttLogs
{
    static class Program
    {
        /// <summary>
        ///The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            //Application.Run(new AttLogsMain());

            //Write Log
            string messages = "[" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") +"] - [INF] : " + "Apllication is starting.";
            CommonLibrary.Instance.writeLogApp(messages);
            //Write Log

            //Check appliation is running 
            _OnStartup();

            ProcessIcon pi = new ProcessIcon();
            pi.Display();
            Application.Run();

            //string CompanyId = CommonLibrary.Instance.getCompanyId();
            //if (CompanyId != "0") {
            //    ProcessIcon pi = new ProcessIcon();
            //    pi.Display();
            //    Application.Run();
            //}
            //else {
            //    messages = "[" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "] - [INF] : " + "Your Company isn't registered.";
            //    CommonLibrary.Instance.writeLogApp(messages);
            //    MessageBox.Show("Your Company isn't registered.","Info");

            //    messages = "[" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "] - [INF] : " + "Close.";
            //    CommonLibrary.Instance.writeLogApp(messages);
            //    Application.Exit();
            //}
        }

        static bool IsProcessOpen(string name)
        {
            foreach (Process clsProcess in Process.GetProcesses())
            {
                if (clsProcess.ProcessName.Contains(name))
                {
                    return true;
                }
            }

            return false;
        }

        static void _OnStartup()
        {
            // Get Reference to the current Process
            Process thisProc = Process.GetCurrentProcess();
            //string appName = System.AppDomain.CurrentDomain.FriendlyName.ToString();
            
            if (IsProcessOpen(thisProc.ProcessName) == false)
            {
                //MessageBox.Show("Application not open!");
                //System.Windows.Application.Current.Shutdown();
            }
            else
            {
                // Check how many total processes have the same name as the current one
                //int totalRunning = Process.GetProcessesByName(thisProc.ProcessName).Length;
                //MessageBox.Show(totalRunning.ToString());

                if (Process.GetProcessesByName(thisProc.ProcessName).Length > 1)
                {
                    // If ther is more than one, than it is already running.
                    MessageBox.Show("Application Personalia Attendance is already running on try icon.");
                    Application.Exit();
                    Environment.Exit(0);
                    //return;
                }
            }
        }
    }
}